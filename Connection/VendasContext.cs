using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using payment_api.Models;

namespace payment_api.Connection
{
    public class VendasContext : DbContext
    {
        public VendasContext(DbContextOptions<VendasContext> options) : base(options){

        }

        public DbSet<Venda> Vendas {get; set;}
        public DbSet<Vendedor> Vendedor {get; set;}
    }
}
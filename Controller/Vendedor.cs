using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using payment_api.Connection;
using payment_api.Models;

namespace payment_api.Controller
{
    [Route("[controller]")]
    public class Vendedores : ControllerBase
    {
        private VendasContext _vendasContext;

        public Vendedores(VendasContext vendas)
        {
            _vendasContext = vendas;
        }

        [HttpPost]
        [Route("")]
        public IActionResult CriaVendedor([FromBody] Vendedor vendedor)
        {
            _vendasContext.Vendedor.Add(vendedor);
            _vendasContext.SaveChanges();

            return Ok();
        }

        [HttpGet("{Id}")]
        public IActionResult BuscaVendedor(int Id){
            var vendedor = _vendasContext.Vendedor.Find(Id);

            if(vendedor == null){
                return NotFound();
            }

            return Ok(vendedor);
        }

        [HttpDelete("{Id}")]
        public IActionResult DeletaVendedor(int Id){
            var vendedor = _vendasContext.Vendedor.Find(Id);

            if(vendedor == null) {
                return NotFound();
            }

            _vendasContext.Vendedor.Remove(vendedor);
            _vendasContext.SaveChanges();

            return NoContent();
        }


        [HttpPut("{Id}")]
        public IActionResult AtualizaVendedor(int Id, Vendedor vendedor){
            var _vendedor = _vendasContext.Vendedor.Find(Id);

            if(vendedor == null) {
                return NotFound();
            }

            _vendedor.Cpf = vendedor.Cpf;
            _vendedor.Email = vendedor.Email;
            _vendedor.Nome = vendedor.Nome;
            _vendedor.Telefone = vendedor.Telefone;

            _vendasContext.Vendedor.Update(_vendedor);
            _vendasContext.SaveChanges();

            return Ok(_vendedor);
        }

    }
}
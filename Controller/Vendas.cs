using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.Extensions.Logging;
using payment_api.Connection;
using payment_api.Models;

namespace payment_api.Controller
{
    [Route("[controller]")]
    public class Vendas : ControllerBase
    {
        private VendasContext _context;
        public Vendas(VendasContext context)
        {
            _context = context;
        }


        [HttpPost]
        public IActionResult RealizaVenda([FromBody] VendaInputModel vendaInput)
        {

            var vendedor = _context.Vendedor.FirstOrDefault(v => v.Id == vendaInput.VendedorId);

            if (vendedor == null)
            {
                return BadRequest("Vendedor não encontrado");
            }

            var venda = new Venda
            {

                Vendedor = vendedor,
                Data = DateTime.Now,
                Status = "Aguardando pagamento",
                Item = vendaInput.Item
            };

            if (string.IsNullOrWhiteSpace(vendaInput.Item))
            {
                return BadRequest("O campo Item não pode conter apenas espaços em branco.");
            }

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscaVendaPorId), new { id = venda.Id }, venda);
        }



        [HttpGet("{id}")]
        public IActionResult BuscaVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
            {
                return NotFound("Venda não encontrada");
            }

            return Ok(venda);
        }

        [HttpPut]
        public IActionResult AtualizaVenda([FromBody] string novoStatus, int Id)
        {
            var vendaBanco = _context.Vendas.Find(Id);

            if (vendaBanco == null)
            {
                return NotFound("Venda não encontrada");
            }

            string novoStatusValido = null;
            switch (vendaBanco.Status)
            {
                case "Aguardando pagamento":
                    novoStatusValido = novoStatus == "Pagamento Aprovado" || novoStatus == "Cancelada" ? novoStatus : null;
                    break;
                case "Pagamento Aprovado":
                    novoStatusValido = novoStatus == "Enviado para Transportadora" || novoStatus == "Cancelada" ? novoStatus : null;
                    break;
                case "Enviado para Transportador":
                    novoStatusValido = novoStatus == "Entregue" ? novoStatus : null;
                    break;
            }

            if (novoStatusValido == null)
            {
                return BadRequest("Transição de status inválida");
            }

            vendaBanco.Status = novoStatusValido;
            _context.SaveChanges();
            return Ok(vendaBanco);
        }


        [HttpDelete]
        public IActionResult DeletaVenda(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound("Venda não encontrada");
            }

            _context.Vendas.Remove(venda);
            _context.SaveChanges();
            return NoContent();
        }
    }
}
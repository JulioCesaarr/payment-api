using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace payment_api.Models
{
    public class Venda
    {
        public string Status { get; set; }
        public DateTime Data { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage = "O Campo Item é obrigatório")]
        [RegularExpression(@"^\S*$", ErrorMessage = "O campo Item não pode conter apenas espaços em branco.")]
        public string Item { get; set; }
        public Vendedor Vendedor {get; set;}
    }
}
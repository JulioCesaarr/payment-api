using System;
using System.ComponentModel.DataAnnotations;

namespace payment_api.Models
{
    public class VendaInputModel
    {

        [Required(ErrorMessage = "O campo VendedorId é obrigatório")]
        public int VendedorId { get; set; }

        public int VendaId {get; set; }

        [Required(ErrorMessage = "O campo Item é obrigatório")]
        [RegularExpression(@"^\S*$", ErrorMessage = "O campo Item não pode conter apenas espaços em branco.")]
        public string Item { get; set; }
        
        [Required(ErrorMessage = "O campo Data é obrigatório")]
        public DateTime Data { get; set; }

        [Required(ErrorMessage = "O campo Status é obrigatório")]
        public string Status { get; set; }
        
        public VendaInputModel(Venda venda)
        {
            VendedorId = venda.Vendedor.Id;
            Item = venda.Item;
            Data = venda.Data;
            Status = venda.Status;
        }
        public VendaInputModel()
        {
            
        }
    }
}
